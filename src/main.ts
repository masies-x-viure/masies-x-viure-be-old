import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  /* Throw errors when whitelisted properties are found */
  app.useGlobalPipes(new ValidationPipe({
    transform: true, // => Auto-transform Payloads to DTO instances
    whitelist: true, // Enabling "whitelist" feature of ValidationPipe
    forbidNonWhitelisted: true, //Throw errors when whitelisted properties are found
    transformOptions: {
      enableImplicitConversion: true
    }
  }))

  await app.listen(3000);
}
bootstrap();
