import { IsBoolean, IsString } from 'class-validator';

export class CreateGroupDto {
  @IsString({ each: true })
  readonly name: string[]

  @IsString()
  readonly email: string

  @IsString()
  readonly groupDescription: string

  @IsBoolean()
  readonly isGroup: boolean

  @IsString()
  readonly lookingFor: string

  @IsString({ each: true })
  readonly locations: string[]

  @IsString({ each: true })
  readonly houseFeature: string[]

  @IsBoolean()
  readonly workProject: boolean

  @IsString()
  readonly workProjectDesc: string
}
