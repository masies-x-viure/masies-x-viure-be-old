import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateGroupDto } from './dto/create-group.dto';
import { InjectModel } from '@nestjs/mongoose';
import { Group } from './entities/group.entity';
import { Model } from 'mongoose';
import { UpdateGroupDto } from './dto/update-group.dto';

@Injectable()
export class GroupsService {
  constructor(
    @InjectModel(Group.name) private readonly groupModel: Model<Group>
  ) {}

  getAll() {
    return this.groupModel.find().exec()
  }
  async getById(id: string) {
    const group = await this.groupModel.findOne({ _id: id}).exec()

    if (!group) {
      throw new NotFoundException(`Group #${id} not found`)
    }
    return group
  }

  create(createGroupDto: CreateGroupDto) {
    const coffee = new this.groupModel(createGroupDto)
    return coffee.save();
  }

  async remove(id: string) {
    const group = await this.getById(id);
    const resp = group.remove();
    console.log('\n\ngroup.remove();:  ', resp);
    return resp
  }

  async update(id: string, updateGroupDto: UpdateGroupDto) {
    const group = await this.groupModel
      .findOneAndUpdate({ _id: id }, { $set: updateGroupDto }, { new: true })
      .exec();

    if (!group) {
      throw new NotFoundException(`Coffee #${id} not found`);
    }
    return group;
  }
}
