import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import {Document} from 'mongoose';

@Schema()
export class Group extends Document {
  @Prop({ required: true, type: [String] })
  name: string[]

  @Prop({ required: true, type: String })
  email: string

  @Prop({ required: true, type: String })
  groupDescription: string

  @Prop({ required: true, type: Boolean })
  isGroup: boolean

  @Prop({ required: true, type: String })
  lookingFor: string

  @Prop({ required: true, type: [String] })
  locations: string[]

  @Prop({ required: true, type: [String] })
  houseFeature: string[]

  @Prop({ required: true, type: Boolean })
  workProject: boolean

  @Prop({ required: true, type: String })
  workProjectDesc: string
}

export const GroupSchema = SchemaFactory.createForClass(Group)
